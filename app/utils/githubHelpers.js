import * as axios from 'axios';

const ID = "YOUR_CLIENT_ID";
const SEC = "YOUR_SECRET_ID";

function getUserInfo(username) {
    return axios.get(`https://api.github.com/users/${username}?client_id=${ID}&client_secret=${SEC}`);
}

export function getPlayersInfo(players) {
    return axios.all(players.map((username) => {
        return getUserInfo(username);
    }))
    .then((info) => {
        return info.map((user) => {
            return user.data;
        })
    })
    .catch((err) => {
        console.warn(`Error in getPlayersInfo: ${err}`);
    });
}
