import React from 'react';
import Prompt from '../components/Prompt';

class PromptContainer extends React.Component {
    constructor() {
        super();
        this._handleSubmitUser = this._handleSubmitUser.bind(this);
        this._handleUpdateUser = this._handleUpdateUser.bind(this);

        this.state = {
            username: ''
        }
    }

    _handleSubmitUser(e) {
        e.preventDefault();
        var username = this.state.username;
        this.setState({
            username: ''
        });

        if (this.props.routeParams.playerOne) {
            var pathname =
            this.context.router.push({
                pathname: '/battle',
                query: {
                    playerOne: this.props.routeParams.playerOne,
                    playerTwo: this.state.username,
                }
            });
        } else {
            this.context.router.push('/playerTwo/' + this.state.username)
        }
    }

    _handleUpdateUser(e) {
        this.setState({
            username: e.target.value
        });
    }

    render() {
        return (
            <Prompt
                onSubmitUser={this._handleSubmitUser}
                onUpdateUser={this._handleUpdateUser}
                header={this.props.route.header}
                username={this.state.username} />
        );
    }
};

PromptContainer.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default PromptContainer;
